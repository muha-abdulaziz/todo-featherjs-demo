# todo-app

> TODO app

## About

A simple todo app uses [Feathers](http://feathersjs.com) framework.

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Make sure you have mongodb installed. (you can use the compose file).
2. Install your dependencies

    ```
    cd path/to/todo-app
    npm install
    ```

3. Start your app

    ```
    npm run compile # Compile TypeScript source
    npm start
    ```

## Documentation

You can find the postman collection.
