// For more information about this file see https://dove.feathersjs.com/guides/cli/service.class.html#database-services
import type { Paginated, PaginationOptions, Params } from '@feathersjs/feathers'
import { AdapterId, MongoDBService, NullableAdapterId } from '@feathersjs/mongodb'
import type { MongoDBAdapterParams, MongoDBAdapterOptions } from '@feathersjs/mongodb'

import type { Application } from '../../declarations'
import type { Todo, TodoData, TodoPatch, TodoQuery } from './todo.schema'
import { app } from '../../app'
import { NotFound } from '@feathersjs/errors/lib'

export type { Todo, TodoData, TodoPatch, TodoQuery }

export interface TodoParams extends MongoDBAdapterParams<TodoQuery> {}

// By default calls the standard MongoDB adapter service methods but can be customized with your own functionality.
export class TodoService<ServiceParams extends Params = TodoParams> extends MongoDBService<
  Todo,
  TodoData,
  TodoParams,
  TodoPatch
> {
  async find(params: any): Promise<any> {
    params.query = {...params.query, createdBy: params.user?._id}
    return super.find(params)
  }

  async create(data: any, params: TodoParams): Promise<any> {
    return super.create({
      ...data,
      createdBy: params.user?._id,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  async get(id: AdapterId, params: TodoParams) {
    const todo = await super.get(id);
    if (todo.createdBy.toString() !== params.user?._id.toString()) {
      throw new NotFound(`No record found for id '${id}'`)
    }

    return todo;
  }

  async remove(id: NullableAdapterId, params: TodoParams): Promise<any> {
    if (!id) throw new NotFound(`No record found for id '${id}'`)

    const todo = await super.get(id);
    if (todo.createdBy.toString() !== params.user?._id.toString()) {
      throw new NotFound(`No record found for id '${id}'`)
    }

    return super.remove(id);
  }

  async patch(id: NullableAdapterId, data: any, params: TodoParams): Promise<any> {
    if (!id) throw new NotFound(`No record found for id '${id}'`)

    const todo = await super.get(id);
    if (todo.createdBy.toString() !== params.user?._id.toString()) {
      throw new NotFound(`No record found for id '${id}'`)
    }

    return super.patch(id, data);
  }

}

export const getOptions = (app: Application): MongoDBAdapterOptions => {
  return {
    paginate: app.get('paginate'),
    Model: app.get('mongodbClient').then(db => db.collection('todo'))
  }
}
