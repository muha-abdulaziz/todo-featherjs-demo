FROM node:18-alpine AS builder

# there is a bug in the current npm
# TODO: remove the following line after the bug fixed
RUN npm install --global npm
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
# build the project
RUN npm install && npm run compile
# remove "node_modules" that contain dev dependencies
# remove typescript files
RUN rm -rf node_modules src
# clean installation
RUN npm ci --omit=dev && npm prune --production


FROM node:18-alpine AS runner
WORKDIR /usr/src/app
RUN chown node /usr/src/app
USER node
COPY --chown=node --from=builder /usr/src/app/ /usr/src/app/

EXPOSE 3030
CMD ["node", "lib/"]
